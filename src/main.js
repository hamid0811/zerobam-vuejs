import './assets/css/main.scss';
import 'bootstrap/dist/css/bootstrap.css';
import './bootstrap';
import 'material-design-icons/iconfont/material-icons.css';
import Vue from 'vue';
import Router from 'vue-router';

import App from'./App.vue';
import Routes from './routes/routes';
import { store } from './store';
import './registerServiceWorker';

import VueKeepScrollPosition from 'vue-keep-scroll-position';
import VModal from 'vue-js-modal';
import ToggleButton from 'vue-js-toggle-button';
import VueLocalForage from 'vue-localforage';
import VueLazyload from 'vue-lazyload';

var VueScrollTo = require('vue-scrollto');
import Vlf from 'vlf'

window._ = require('vlf');

Vue.use(Vlf);
Vue.use(VueLazyload, {
  preLoad: 1.3,
  loading: require('./assets/images/loading.png'),
  attempt: 1
})
Vue.use(VueLocalForage);
Vue.use(ToggleButton);
Vue.use(VueScrollTo);
Vue.use(VModal);
Vue.use(VueKeepScrollPosition);
Vue.use(Router);
window.Event = new Vue();


const router = new Router({
    mode:'history',
    base:'/',
    fallback: true,
    routes: Routes,
    scrollBehavior (to, from, savedPosition) {
        if ('scrollRestoration' in history) { history.scrollRestoration = 'manual'; }
        return new Promise((resolve, reject) => {
            Event.$on('scrollBeforeEnter', ()=>{
                resolve(savedPosition || { x: 0, y: 0 })
            });
        })
    }
});


var eventHub = new Vue();

new Vue({
  mounted(){
  },
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')

